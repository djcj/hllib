version = 2.4.5
major   = 2
minor   = 4

PREFIX     ?= /usr/local
bindir     ?= $(PREFIX)/bin
libdir     ?= $(PREFIX)/lib
includedir ?= $(PREFIX)/include

CC  := gcc
CXX := g++
LD  := g++
AR  := ar
RANLIB       := ranlib
INSTALL_PROG := install -m 755
INSTALL_DATA := install -m 644
MKDIR_P      := mkdir -p
LN_S         := ln -s


ifeq ($(DEBUG),1)
CFLAGS   ?= -O2 -g
CXXFLAGS ?= -O2 -g
else
CFLAGS   ?= -O3
CXXFLAGS ?= -O3
endif
CXXFLAGS += -funroll-loops -fvisibility=hidden -std=c++11
HLLib/WADFile.o: CXXFLAGS+=-Wno-maybe-uninitialized


ifeq ($(TARGETOS),)
getos = \
case "$$(uname -s)" in \
  CYGWIN*|MINGW*|MSYS*) echo 'Windows';; \
  Darwin) echo 'Darwin';; \
  *) echo 'Unix';; \
esac
TARGETOS = $(shell echo "$$($(getos))")
endif


ifeq ($(TARGETOS),Windows)
LDFLAGS_shared = -shared
endif
ifeq ($(TARGETOS),Darwin)
LDFLAGS_shared = -dynamiclib -install_name $@ -compatibility_version $(major).$(minor) -current_version $(version)
endif
ifeq ($(TARGETOS),Unix)
LDFLAGS_shared = -shared -Wl,-soname,$@ -fPIC
endif

main = hlextract
ifeq ($(TARGETOS),Windows)
main = HLExtract.exe
endif
srcs_main = HLExtract/Main.c
objs_main = HLExtract/Main.o

srcs_lib = $(wildcard HLLib/*.cpp)
objs_lib = $(srcs_lib:.cpp=.o)
objs_pic = $(srcs_lib:.cpp=.pic_o)

lib_so         = libhl.so
lib_static     = libhl.a
lib_static_pic = libhl_pic.a
lib_shared     = $(lib_so).$(major)
objs_shared    = $(objs_lib)
ifeq ($(TARGETOS),Windows)
lib_shared = HLLib.dll
lib_static = HLLib.lib
endif
ifeq ($(TARGETOS),Darwin)
lib_shared = libhl.$(major).dylib
endif
ifeq ($(TARGETOS),Unix)
objs_shared = $(objs_pic)
endif
lib = $(lib_shared)
ifeq ($(STATIC),1)
lib = $(lib_static)
endif


TARGETS = $(main) $(lib_shared) $(lib_static)
ifneq ($(TARGETOS),Windows)
TARGETS += $(lib_static_pic)
endif




all: $(TARGETS)

ifeq ($(TARGETOS),Unix)
install:
	$(MKDIR_P) $(DESTDIR)$(bindir)
	$(MKDIR_P) $(DESTDIR)$(libdir)
	$(MKDIR_P) $(DESTDIR)$(includedir)
	$(INSTALL_PROG) $(main) $(DESTDIR)$(bindir)
	$(INSTALL_DATA) lib/HLLib.h $(DESTDIR)$(includedir)
	$(INSTALL_DATA) $(lib_static) $(DESTDIR)$(libdir)
	$(INSTALL_DATA) $(lib_static_pic) $(DESTDIR)$(libdir)
	$(INSTALL_DATA) $(lib_shared) $(DESTDIR)$(libdir)/$(lib_so).$(version)
	$(LN_S) $(lib_so).$(version) $(DESTDIR)$(libdir)/$(lib_shared)
	$(LN_S) $(lib_so).$(version) $(DESTDIR)$(libdir)/$(lib_so)
endif

clean:
	rm -f $(TARGETS)
	rm -f $(objs_main) $(objs_lib) $(objs_pic)

distclean: clean

$(main): $(objs_main) $(lib)
	$(LD) $(LDFLAGS) -o $@ $^

$(lib_shared): $(objs_shared)
	$(LD) $(LDFLAGS_shared) $(LDFLAGS) -o $@ $^

$(lib_static): $(objs_lib)
	$(AR) cru $@ $^
	$(RANLIB) $@

$(lib_static_pic): $(objs_pic)
	$(AR) cru $@ $^
	$(RANLIB) $@

%.o: %.c
	$(CC) -c -Wall $(CFLAGS) $(CPPFLAGS) -o $@ $<

%.o: %.cpp
	$(CXX) -c -Wall $(CXXFLAGS) -o $@ $<

%.pic_o: %.cpp
	$(CXX) -c -Wall -fPIC -DPIC $(CXXFLAGS) -o $@ $<

